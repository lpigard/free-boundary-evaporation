#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <hdf5.h>
#include <unistd.h>
#include <time.h>

double hp_part(const double phi, const double Nc, const double chi)
{
    return -(1 - phi) * exp(phi * (1. - 1. / Nc + chi * phi));
    // return 0;
}

void write_dataset(hid_t file_id, void *data, const char *name, int rank, hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    hid_t dataset_id = H5Dcreate2(file_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
}

double poly_mass(double const * const phi, const double dx, const double h, const int Nx)
{
    double m = 0;

    for(int i = 0; i < Nx; i++)
    {
        m += phi[i];
    }

    return m * dx * h;
}

double poly_mass_2(double const * const phi, const double dx, const double h, const int Nx)
{
    double m = 0.5 * (phi[0] + phi[Nx - 1]);

    for(int i = 1; i < Nx - 1; i++)
    {
        m += phi[i];
    }

    return m * dx * h;
}

void calc_lambda(double * lambda, double * phi, const double Nc, const int mobility, const int Nx)
{
    switch(mobility)
    {
        case 0:
        for(int i = 0; i < Nx; i++)
        {
            lambda[i] = 1;
        }
        break;

        case 1:
        for(int i = 0; i < Nx; i++)
        {
            lambda[i] = phi[i] * (1 - phi[i]);
        }
        break;

        case 2:
        for(int i = 0; i < Nx; i++)
        {
            lambda[i] = 1 / (1 / (Nc * phi[i]) + 1 / (1 - phi[i]));
        }
        break;
    }
}

double calc_amax(double * phi, double * lambda, const double Nc, const double chi, const int Nx)
{
    double amax = 1;

    for(int i = 0; i < Nx; i++)
    {
        const double a = fabs(1 / (Nc * phi[i]) + 1 / (1 - phi[i]) - 2 * chi) * lambda[i];

        if(a > amax)
        {
            amax = a;
        }
    }

    return amax;
}

int main(int argc, char ** argv)
{
    assert(argc == 6);

    const double phi0 = atof(argv[1]);
    const double phit = atof(argv[2]);
    const double h0 = atof(argv[3]);
    const double chi = atof(argv[4]);
    const int mobility = atoi(argv[5]);

    double h = h0;
    double dx = 0.2 * 1. / h0;

    int Nx = ceil(1. / dx) + 1;
    int Nx0 = 2;

    while(Nx0 < Nx)
    {
        Nx0 = 2 * Nx0 - 1;
    }

    Nx = Nx0;
    double hlast = h;

    dx = 1. / (Nx - 1);

    const double tmax = 1e4;
    double t = 0;

    const int dt_ana = 10000;
    const double gamma = 1;
    const double Nc = 32;

    double * phi_cp = malloc(Nx * sizeof *phi_cp);
    double * phi = malloc(Nx * sizeof *phi);
    double * mu = malloc(Nx * sizeof *mu);
    double * z = malloc(Nx * sizeof *z);
    double * lambda = malloc(Nx * sizeof *lambda);

    srand(time(NULL));   // Initialization, should only be called once.

    for(int i = 0; i < Nx; i++)
    {
        const double r = (double)rand() / RAND_MAX - 0.5;
        phi[i] = phi0 * (1 + 0.000 * r);
    }

    // for(int i = 0; i < Nx; i++)
    // {
    //     const double r = (double)rand() / RAND_MAX - 0.5;
    //     if(i < Nx / 2)
    //     {
    //         phi[i] = 0.2 * (1 + 0.001 * r);
    //     }
    //     else
    //     {
    //         phi[i] = 0.1 * (1 + 0.00 * r);
    //     }
    //
    // }

    const double m0 = poly_mass(phi, dx, h, Nx);
    const double m0_2 = poly_mass_2(phi, dx, h, Nx);

    char fname[1024];
    sprintf(fname, "flux_phi0%g_phit%g_mobility%d.dat", phi0, phit, mobility);
    FILE * f = fopen(fname, "w");

    long int it = 0;

    printf("dx=%g; NC=%g e9\n", dx, (double)Nx * tmax / 1e9);

    while(t < tmax)
    {
        const double hp = hp_part(phi[Nx - 1], Nc, chi) - hp_part(phit, Nc, chi);
        calc_lambda(lambda, phi, Nc, mobility, Nx);

        for(int i = 0; i < Nx; i++)
        {
            mu[i] = log(phi[i]) / Nc - log(1 - phi[i]) - 2 * chi * phi[i];
        }

        const double phi_0m1 = phi[1];
        const double phi_0m2 = phi[2];
        const double phi_N = 2 * dx * h * (-hp * phi[Nx - 1] / lambda[Nx - 1]) / (1 / (phi[Nx - 1] * Nc) + 1 / (1 - phi[Nx - 1]) - 2 * chi) + phi[Nx - 2];
        const double phi_Np1 = 2 * phi_N - 2 * phi[Nx - 2] + phi[Nx - 3];

        const double mu_0m1 = log(phi_0m1) / Nc - log(1 - phi_0m1) - 2 * chi * phi_0m1;
        const double mu_N = log(phi_N) / Nc - log(1 - phi_N) - 2 * chi * phi_N;

        double lambda_0m1, lambda_N;

        switch(mobility)
        {
            case 0:
            lambda_0m1 = 1;
            lambda_N = 1;
            break;

            case 1:
            lambda_0m1 = phi_0m1 * (1 - phi_0m1);
            lambda_N = phi_N * (1 - phi_N);
            break;

            case 2:
            lambda_0m1 = 1 / (1 / (Nc * phi_0m1) + 1 / (1 - phi_0m1));
            lambda_N = 1 / (1 / (Nc * phi_N) + 1 / (1 - phi_N));
            break;
        }

        double rhs;
        const double amax = calc_amax(phi, lambda, Nc, chi, Nx);
        const double dt = 0.2 / amax * dx * dx * dx * dx * h * h * h * h;

        for(int i = 2; i < Nx - 2; i++)
        {
            rhs =   lambda[i] * ((mu[i + 1] + mu[i - 1] - 2 * mu[i]) / (dx * dx * h * h)
                    - gamma * (phi[i - 2] - 4 * phi[i - 1] + 6 * phi[i] - 4 * phi[i + 1] + phi[i + 2]) / (dx * dx * dx * dx * h * h * h * h))
                    + dx * i * hp * (phi[i + 1] - phi[i - 1]) / (2 * dx * h)
                    + (lambda[i + 1] - lambda[i - 1]) * (mu[i + 1] - mu[i - 1]) / (4 * dx * dx * h * h);
            phi_cp[i] = phi[i] + dt * rhs;
        }

        rhs =   lambda[0] * ((mu[1] + mu_0m1 - 2 * mu[0]) / (dx * dx * h * h)
                - gamma * (phi_0m2 - 4 * phi_0m1 + 6 * phi[0] - 4 * phi[1] + phi[2]) / (dx * dx * dx * dx * h * h * h * h))
                + dx * 0 * hp * (phi[1] - phi_0m1) / (2 * dx * h)
                + (lambda[1] - lambda_0m1) * (mu[1] - mu_0m1) / (4 * dx * dx * h * h);
        phi_cp[0] = phi[0] + dt * rhs;

        rhs =   lambda[1] * ((mu[2] + mu[0] - 2 * mu[1]) / (dx * dx * h * h)
                - gamma * (phi_0m1 - 4 * phi[0] + 6 * phi[1] - 4 * phi[2] + phi[3]) / (dx * dx * dx * dx * h * h * h * h))
                + dx * 1 * hp * (phi[2] - phi[0]) / (2 * dx * h)
                + (lambda[2] - lambda[0]) * (mu[2] - mu[0]) / (4 * dx * dx * h * h);
        phi_cp[1] = phi[1] + dt * rhs;

        rhs =   lambda[Nx - 2] * ((mu[Nx - 1] + mu[Nx - 3] - 2 * mu[Nx - 2]) / (dx * dx * h * h)
                - gamma * (phi[Nx - 4] - 4 * phi[Nx - 3] + 6 * phi[Nx - 2] - 4 * phi[Nx - 1] + phi_N) / (dx * dx * dx * dx * h * h * h * h))
                + dx * (Nx - 1) * hp * (phi[Nx - 1] - phi[Nx - 3]) / (2 * dx * h)
                + (lambda[Nx - 1] - lambda[Nx - 3]) * (mu[Nx - 1] - mu[Nx - 3]) / (4 * dx * dx * h * h);
        phi_cp[Nx - 2] = phi[Nx - 2] + dt * rhs;

        rhs =   lambda[Nx - 1] * ((mu_N + mu[Nx - 2] - 2 * mu[Nx - 1]) / (dx * dx * h * h)
                - gamma * (phi[Nx - 3] - 4 * phi[Nx - 2] + 6 * phi[Nx - 1] - 4 * phi_N + phi_Np1) / (dx * dx * dx * dx * h * h * h * h))
                + dx * (Nx - 1) * hp * (phi_N - phi[Nx - 2]) / (2 * dx * h)
                + (lambda_N - lambda[Nx - 2]) * (mu_N - mu[Nx - 2]) / (4 * dx * dx * h * h);
        phi_cp[Nx - 1] = phi[Nx - 1] + dt * rhs;

        if(it % dt_ana == 0)
        {
            const double m1 = poly_mass(phi, dx, h, Nx);
            const double m1_2 = poly_mass_2(phi, dx, h, Nx);

            fprintf(f, "%.12g %g %g %g %g\n", t, h, phi[Nx - 1], hp, m1_2);
            printf("%.12g %g %g %g %g %g\n", t, h, phi[Nx - 1], hp, m1 / m0, m1_2 / m0_2);

            char fn[1024];
            sprintf(fn, "phi0%g_phit%g_mobility%d_ana_fields.h5", phi0, phit, mobility);
            hid_t file_id;

            if(access(fn, F_OK) == 0)
            {
                file_id = H5Fopen(fn, H5F_ACC_RDWR, H5P_DEFAULT);
            }
            else
            {
                file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
            }

            hsize_t d1[1] = {Nx};

            char gn[1024];
            sprintf(gn, "/%.12g", t);
            hid_t group_id = H5Gcreate(file_id, gn, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

            for(int i = 0; i < Nx; i++)
            {
                z[i] = i * dx * h;
            }

            for(int i = 1; i < Nx - 1; i++)
            {
                mu[i] += -gamma * (phi[i + 1] + phi[i - 1] - 2 * phi[i]) / (dx * dx * h * h);
            }

            mu[0] += -gamma * (phi[1] + phi_0m1 - 2 * phi[0]) / (dx * dx * h * h);
            mu[Nx - 1] += -gamma * (phi_N + phi[Nx - 2] - 2 * phi[Nx - 1]) / (dx * dx * h * h);

            write_dataset(group_id, z, "z", 1, d1, H5T_NATIVE_DOUBLE);
            write_dataset(group_id, phi, "phi", 1, d1, H5T_NATIVE_DOUBLE);
            write_dataset(group_id, mu, "mu", 1, d1, H5T_NATIVE_DOUBLE);

            H5Gclose(group_id);
            H5Fclose(file_id);
        }

        memcpy(phi, phi_cp, Nx * sizeof(double));

        t += dt;
        h += hp * dt;
        it++;

        if(h < hlast / 2 && Nx > 4)
        {
            for(int i = 0; i < Nx; i++)
            {
                phi_cp[i] = phi[i];
            }

            free(phi);
            free(mu);
            free(z);
            free(lambda);

            Nx = Nx / 2 + 1;
            dx *= 2;

            phi = malloc(Nx * sizeof *phi);
            mu = malloc(Nx * sizeof *mu);
            z = malloc(Nx * sizeof *z);
            lambda = malloc(Nx * sizeof *lambda);

            for(int i = 0; i < Nx; i++)
            {
                phi[i] = phi_cp[2 * i];
            }

            free(phi_cp);
            phi_cp = malloc(Nx * sizeof *phi_cp);
            hlast = h;
        }

        // printf("error for dn=%g\n", fabs(1 - n0 / n1));
        //
        // printf("%d %g %g %g %g\n", it, t, tmax, hp, h);
    }

    fclose(f);
    free(phi_cp);
    free(phi);
    free(mu);
    free(z);
    free(lambda);
}
